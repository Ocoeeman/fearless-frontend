import './App.css';
import Nav from './nav';


function App({attendees}) {
  if(attendees === undefined) return null;
  return (
    <>
      <Nav />
      <div className="App container">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Conference</th>
            </tr>
          </thead>
          <tbody>
            {attendees.map(attendee=>{
              return (
                <tr key={attendee.href}>
                  <td>{attendee.name}</td>
                  <td>{attendee.conference.name}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </>

  );
}

export default App;
