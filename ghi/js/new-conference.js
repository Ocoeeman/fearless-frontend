window.addEventListener("DOMContentLoaded", async ()=>{
    const url = "http://localhost:8000/api/locations/";
    const response = await fetch(url);
    if(!response.ok){
        console.error("error loading locations");
    } else {
        const data = await response.json();
        const selectTag = document.querySelector("#location");
        for (let location of data.locations){
            const {name, id} = location;
            const option = document.createElement('option');
            option.value = id;
            option.innerHTML = name;
            selectTag.appendChild(option);
        }
        const formTag = document.querySelector("#create-conference-form");
        formTag.addEventListener('submit', async (event)=>{
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const URL = "http://localhost:8000/api/conferences/";
            const fetchConfig = {
                method: 'post',
                body: json,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            const response = await fetch(URL, fetchConfig);
            if(!response.ok){
                console.error("error submitting form")
            } else {
                formTag.reset()
                const newConference = await response.json();
                console.log(newConference);
            }
        });
    }
});
