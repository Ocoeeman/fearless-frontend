function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="col-4 align-self-start">
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            ${startDate} - ${endDate}
        </div
      </div>
    </div>
    `;
  }


function createAlert(){
    return(
        `<div class="alert alert-warning" role="alert">
            <i class="bi bi-exclamation-triangle"></i>
            Somthing went wrong, please reload the page
        </div>`
    )
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try{
        // throw new Error("test error");
        const response = await fetch(url);
        if(!response.ok){
        } else {
            const data = await response.json();

            for(let conference of data.conferences){
                const detailURL = `http://localhost:8000${conference.href}`
                // const tempHTML = createPlaceholder();
                // row.innerHTML = tempHTML;
                const detailResponse = await fetch(detailURL);
                if (detailResponse.ok){
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const start = new Date(details.conference.starts);
                    const startDate = `${start.getMonth()+1}/${start.getDate()}/${start.getFullYear()}`;
                    const end = new Date(details.conference.ends);
                    const endDate = `${end.getMonth()+1}/${end.getDate()}/${end.getFullYear()}`;
                    const location = details.conference.location.name;
                    const html= createCard(title, description, pictureUrl, startDate, endDate, location);
                    const row = document.querySelector('.row');
                    row.innerHTML += html;
                }
            }
        }
    } catch(e){
        console.error(e);
        const alert = document.querySelector('.alert');
        alert.innerHTML = createAlert();
    }
});
