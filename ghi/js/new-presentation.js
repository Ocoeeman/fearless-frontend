window.addEventListener("DOMContentLoaded", async () =>{
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (!response){
        console.error("error loading conference data");
    } else {
        const data = await response.json();
        const selectTag = document.querySelector('#conference');
        for (let conference of data.conferences){
            const {name, id} = conference;
            const option = document.createElement('option');
            option.value = id;
            option.innerHTML = name;
            selectTag.appendChild(option);
        }
        const formTag = document.querySelector("#create-presentation-form");
        formTag.addEventListener('submit', async event=>{
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const fetchConfig = {
                method: 'post',
                body: json,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            const conferenceID = selectTag.selectedIndex
            const presentationUrl = `http://localhost:8000/api/conferences/${conferenceID}/presentations/`;
            const response = await fetch(presentationUrl, fetchConfig);
            if(!response.ok){
                console.error("error submitting presentation data")
            } else {
                formTag.reset();
                const newPresentation = await response.json();
                console.log(newPresentation);
            }

        });
    }
});
