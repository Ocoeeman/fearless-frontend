window.addEventListener('DOMContentLoaded', async ()=>{
    const selectTag = document.querySelector('#conference');
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);
    if (!response.ok){
        console.error('error loading conferences');
    } else {
        const data = await response.json();

        for (let conference of data.conferences){
            const {href, name} = conference;
            const option = document.createElement('option');
            option.value = href;
            option.innerHTML = name;
            selectTag.appendChild(option);
        }
        const loadingIcon = document.querySelector('#loading-conference-spinner');
        console.log(loadingIcon.classList);
        if(!loadingIcon.classList.contains('d-none')){
            loadingIcon.classList.add('d-none');
        }
        const  selectConference = document.querySelector('#conference');
        if(selectConference.classList.contains('d-none')){
            selectConference.classList.remove('d-none');
        }

        const formTag= document.querySelector('#create-attendee-form');
        formTag.addEventListener('submit', async (event)=>{
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const URL = "http://localhost:8001/api/attendees/";
            const fetchConfig = {
                method: 'post',
                body: json,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            loadingIcon.classList.toggle('d-none')
            const response = await fetch(URL, fetchConfig);
            if(!response.ok){
                console.error("error submitting form")
            } else {
                formTag.reset()
                const newAttendee = await response.json();
                console.log(newAttendee);
                formTag.classList.toggle('d-none')
                const successMessage = document.querySelector('#success-message');
                successMessage.classList.toggle('d-none');
            }
        });
    }
});
